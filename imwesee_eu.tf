resource "google_storage_bucket" "imwesee_eu" {
    name = "imwesee_eu"
    location = "EU"
    labels = {
        active = "no"
        content = "unspecified"
    }

    lifecycle_rule = {
        action = {
            type = "Delete"
            
        }
        condition = {
            age = 730
        }
    }
}
