resource "google_storage_bucket" "impublic" {
    name = "impublic"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
