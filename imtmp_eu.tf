resource "google_storage_bucket" "imtmp_eu" {
    name = "imtmp_eu"
    location = "EU"
    labels={
        active = "no"
        content = "temp"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 7
        }
    }
}
