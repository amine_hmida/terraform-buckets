resource "google_storage_bucket" "imfkrawstaging_eu" {
    name = "imfkrawstaging_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
