resource "google_storage_bucket" "imatlas" {
    name = "imatlas"
    location = "EU"
    storage_class = "NEARLINE"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
