#!/bin/env python

import os
import json

BCUKETS = ["4e1de271gsutil-test-test_set_wildcard_non_null_cors-bu-055b0e71",
"4e1de271gsutil-test-test_set_wildcard_non_null_cors-bu-9e18272a",
"build-bin",
"imadidasdcm_eu",
"imadloox_eu",
"imanalytics",
"imapn_eu",
"imatlas",
"imcardy_eu",
"imclient_specific_eu",
"imcorp",
"imcreatives",
"imcrt_eu",
"imd_eu",
"imdbm_eu",
"imds_eu",
"imfk_eu",
"imfkrawstaging_eu",
"imgcp_daily_billing",
"imgcs_usage",
"imgdn_eu",
"imgeo_eu",
"imgrapeshot_eu",
"imibehavior_eu",
"imid_correlated_ids",
"imidb_ads",
"imidb_eu_ns",
"imidb_eu_tmp",
"imidb_raw",
"imnielsen",
"impgcl_eu",
"impixelstatic",
"impressiondesk-apidoc",
"impublic",
"imredis_eu",
"imrelease",
"imreports",
"imsalt_eu",
"imscreen6",
"imtableau",
"imtmp_eu",
"imtubemogul_eu",
"imvidiro_eu",
"imweather_eu",
"imwesee_eu",
"test_102",
"tsdb-build"]

TCONF ='''resource "google_storage_bucket" "{name}" {{
    name = "{name}"
    location = "{location}"
}}
'''

if __name__ == "__main__":
    for bucket in BCUKETS:
        os.system("terraform import google_storage_bucket.{} {}".format(bucket, bucket))

    tf = open('terraform.tfstate')
    v = json.load(tf)
    for bucket in BCUKETS:
        try:
            location = v["modules"][0]["resources"]["google_storage_bucket." + bucket]["primary"]["attributes"]["location"]
            with open(bucket + ".tf", "w") as f:
                print(TCONF.format(name=bucket, location=location))
                f.write(TCONF.format(name=bucket, location=location)) 
        except KeyError:
            pass
