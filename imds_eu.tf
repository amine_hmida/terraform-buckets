resource "google_storage_bucket" "imds_eu" {
    name = "imds_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
