resource "google_storage_bucket" "imfk_eu" {
    name = "imfk_eu"
    location = "EU"
    labels={
        active = "no"
        content = "topics"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
