resource "google_storage_bucket" "imapn_eu" {
    name = "imapn_eu"
    location = "EU"
    labels={
        content = "unspecified"
        active = "yes"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
