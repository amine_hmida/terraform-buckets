resource "google_storage_bucket" "build-bin" {
    name = "build-bin"
    location = "US"
    storage_class = "MULTI_REGIONAL"
}
