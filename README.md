# Terraform-buckets

Provisioning Google cloud storage buckets with terraform

## How to use this

Create a new file with a new terraform state.

`terraform plan`

If you are satisfied, then `terraform apply`  

## Terraform syntax for GCS buckets:

https://www.terraform.io/docs/providers/google/r/storage_bucket.html
