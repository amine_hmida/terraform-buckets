resource "google_storage_bucket" "imadidasdcm_eu" {
    name = "imadidasdcm_eu"
    location = "EUROPE-WEST1"
    storage_class = "REGIONAL"
    labels={
        active = "no"
        content = "unspecified"
    }
}

