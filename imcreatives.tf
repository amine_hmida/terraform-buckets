resource "google_storage_bucket" "imcreatives" {
    name = "imcreatives"
    location = "EUROPE-WEST1"
    storage_class = "REGIONAL"
    labels={
        active = "no"
        content = "unspecified"
    }
}
