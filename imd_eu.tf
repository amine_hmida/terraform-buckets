resource "google_storage_bucket" "imd_eu" {
    name = "imd_eu"
    location = "EU"
    labels={
        active = "no"
        content = "logs"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
