resource "google_storage_bucket" "impgcl_eu" {
    name = "impgcl_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
