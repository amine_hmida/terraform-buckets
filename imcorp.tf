resource "google_storage_bucket" "imcorp" {
    name = "imcorp"
    location = "EU"
    storage_class = "NEARLINE"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
