resource "google_storage_bucket" "imscreen6" {
    name = "imscreen6"
    location = "EU"
    storage_class = "NEARLINE"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
