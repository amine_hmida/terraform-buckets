resource "google_storage_bucket" "imid_correlated_ids" {
    name = "imid_correlated_ids"
    location = "EU"
    storage_class = "MULTI_REGIONAL"
    labels={
        active = "no"
        content = "unspecified"
    }
}
