resource "google_storage_bucket" "imcrt_eu" {
    name = "imcrt_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
