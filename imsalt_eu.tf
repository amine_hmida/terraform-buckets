resource "google_storage_bucket" "imsalt_eu" {
    name = "imsalt_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
