resource "google_storage_bucket" "imibehavior_eu" {
    name = "imibehavior_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
