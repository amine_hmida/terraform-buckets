resource "google_storage_bucket" "imidb_eu_ns" {
    name = "imidb_eu_ns"
    location = "EU"
    storage_class = "NEARLINE"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
