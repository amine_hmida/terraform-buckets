resource "google_storage_bucket" "imgcp_daily_billing" {
    name = "imgcp_daily_billing"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
