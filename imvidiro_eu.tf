resource "google_storage_bucket" "imvidiro_eu" {
    name = "imvidiro_eu"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
