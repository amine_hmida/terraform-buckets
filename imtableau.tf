resource "google_storage_bucket" "imtableau" {
    name = "imtableau"
    location = "EU"
    storage_class = "NEARLINE"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
