resource "google_storage_bucket" "impressiondesk-apidoc" {
    name = "impressiondesk-apidoc"
    location = "EU"
    storage_class = "DURABLE_REDUCED_AVAILABILITY"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
