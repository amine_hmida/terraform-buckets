resource "google_storage_bucket" "imidb_eu_tmp" {
    name = "imidb_eu_tmp"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
