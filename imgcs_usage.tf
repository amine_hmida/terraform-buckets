resource "google_storage_bucket" "imgcs_usage" {
    name = "imgcs_usage"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
