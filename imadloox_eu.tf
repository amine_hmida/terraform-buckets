resource "google_storage_bucket" "imadloox_eu" {
    name = "imadloox_eu"
    location = "EU"
    labels={
        active = "no"
        content = "logs"
    }

    lifecycle_rule = {
        action = {
            type = "Delete"
        }
        condition = {
            age = 730
        }
    }
}
