resource "google_storage_bucket" "impixelstatic" {
    name = "impixelstatic"
    location = "US"
    storage_class = "MULTI_REGIONAL"
    labels={
        active = "no"
        content = "unspecified"
    }
}
