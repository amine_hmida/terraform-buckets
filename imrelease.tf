resource "google_storage_bucket" "imrelease" {
    name = "imrelease"
    location = "EU"
    labels={
        active = "no"
        content = "unspecified"
    }
}
