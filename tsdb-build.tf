resource "google_storage_bucket" "tsdb-build" {
    name = "tsdb-build"
    location = "EU"
    storage_class = "MULTI_REGIONAL"
}
