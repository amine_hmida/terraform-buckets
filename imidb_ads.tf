resource "google_storage_bucket" "imidb_ads" {
    name = "imidb_ads"
    location = "US"
    labels={
        active = "no"
        content = "unspecified"
    }
}
